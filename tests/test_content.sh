#!/bin/bash

if [ -z "`docker exec web sh -c "cat /usr/share/nginx/html/index.html"`" ]; then 	
		
		echo "index.html file is empty!"
		exit 1 	
fi

if [ -z "`docker exec web sh -c "ls -l /usr/share/nginx/html/images/ | grep .jpg"`" ]; then
		
		echo "There is no *.jpg file in /image directory"
		exit 1		
fi
